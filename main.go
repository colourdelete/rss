package main

import "cdel-rss/pkg/app"

func main() {
	a := app.NewApp("dev.cdel.rss")
	w := a.NewWin()
	w.RefreshCurrentFeed()
	w.ShowAndRun()
}

package app

import (
	"cdel-rss/pkg/win"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
)

type App struct {
	app fyne.App
}

func NewApp(id string) App {
	return App{app: app.NewWithID(id)}
}

func (a App) NewWin() *win.Window {
	return win.NewWindow(a.app)
}

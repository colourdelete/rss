package _const

// once the new go embed thing is out, embed the readme!

const About = `
# Coloudelete Feeds

By Ken Shibata (colourdelete)

A simple Feeds reader using the Fyne UI Toolkit and github.com/mmcdole/gofeed.
Available on GitLab.com: gitlab.com/colourdelete/rss.

## Features

- View a single feed
- No images
- No HTML

Licensed under the MIT License. See the file named "LICENSE" for license information.`

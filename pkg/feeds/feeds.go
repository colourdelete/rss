package feeds

import (
	"context"
	"encoding/json"
	"time"

	"github.com/mmcdole/gofeed"
)

type Feeds map[string]Feed

func (f Feeds) From(data []byte) (Feeds, error) {
	feeds_ := Feeds{}
	err := json.Unmarshal(data, &feeds_)
	if err != nil {
		return f, err
	}
	for k, v := range feeds_ {
		f[k] = v // merge; feeds_ has priority
	}
	return feeds_, err
}

func (f Feeds) To() ([]byte, error) {
	return json.Marshal(&f)
}

type Feed struct {
	Name     string        `json:"name"`
	Link     string        `json:"link"`
	Interval time.Duration `json:"interval"`
	Priority int           `json:"priority"`
	Data     *gofeed.Feed  `json:"data"`
}

func (f Feed) Item(idx int) *gofeed.Item {
	if f.Data == nil {
		return nil
	}
	if idx >= len(f.Data.Items) {
		return nil
	}
	return f.Data.Items[idx]
}

func (f *Feed) Get() error {
	// TODO: get timeout from a.Preferences()
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	fp := gofeed.NewParser()
	data, err := fp.ParseURLWithContext(f.Link, ctx)
	if err != nil {
		return err
	}
	f.Data = data
	return nil
}

func (f *Feed) GetForever(ch chan error, stop chan struct{}) {
	ticker := time.NewTicker(f.Interval)
	defer ticker.Stop()
	for {
		select {
		case <-stop:
			return
		case <-ticker.C:
			var err error
			err = f.Get()
			ch <- err
		}
	}
}
